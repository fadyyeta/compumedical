﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Invoice.aspx.cs" Inherits="compuMedical.Invoice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpTitle" runat="server">
    Place an Order
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphead" runat="server">
    <style>
        @media print {
            .noPrint {
                display: none;
            }
        }

        .noMargin {
            padding-left: 0;
        }

        .ohHide {
            display: none;
        }

        #invoice {
            padding: 30px;
        }

        .invoice {
            position: relative;
            background-color: #FFF;
            min-height: 680px;
            padding: 15px;
        }

            .invoice header {
                padding: 10px 0;
                margin-bottom: 20px;
                border-bottom: 1px solid #3989c6;
            }

            .invoice .company-details {
                text-align: right;
            }

                .invoice .company-details .name {
                    margin-top: 0;
                    margin-bottom: 0;
                }

            .invoice .contacts {
                margin-bottom: 20px;
            }

            .invoice .invoice-to {
                text-align: left;
            }

                .invoice .invoice-to .to {
                    margin-top: 0;
                    margin-bottom: 0;
                }

            .invoice .invoice-details {
                text-align: right;
            }

                .invoice .invoice-details .invoice-id {
                    margin-top: 0;
                    color: #3989c6;
                }

            .invoice main {
                padding-bottom: 50px;
            }

                .invoice main .thanks {
                    margin-top: -100px;
                    font-size: 2em;
                    margin-bottom: 50px;
                }

                .invoice main .notices {
                    padding-left: 6px;
                    border-left: 6px solid #3989c6;
                }

                    .invoice main .notices .notice {
                        font-size: 1.2em;
                    }

            .invoice table {
                width: 100%;
                border-collapse: collapse;
                border-spacing: 0;
                margin-bottom: 20px;
            }

                .invoice table td, .invoice table th {
                    padding: 15px;
                    background: #eee;
                    border-bottom: 1px solid #fff;
                }

                .invoice table th {
                    white-space: nowrap;
                    font-weight: 400;
                    font-size: 16px;
                }

                .invoice table td h3 {
                    margin: 0;
                    font-weight: 400;
                    color: #3989c6;
                    font-size: 1.2em;
                }

                .invoice table .qty, .invoice table .total, .invoice table .unit {
                    text-align: right;
                    font-size: 1.2em;
                }

                .invoice table .no {
                    color: #fff;
                    font-size: 1.6em;
                    background: #3989c6;
                }

                .invoice table .unit {
                    background: #ddd;
                }

                .invoice table .total {
                    background: #3989c6;
                    color: #fff;
                }

                .invoice table tbody tr:last-child td {
                    border: none;
                }

                .invoice table tfoot td {
                    background: 0 0;
                    border-bottom: none;
                    white-space: nowrap;
                    text-align: right;
                    padding: 10px 20px;
                    font-size: 1.2em;
                    border-top: 1px solid #aaa;
                }

                .invoice table tfoot tr:first-child td {
                    border-top: none;
                }

                .invoice table tfoot tr:last-child td {
                    color: #3989c6;
                    font-size: 1.4em;
                    border-top: 1px solid #3989c6;
                }

                .invoice table tfoot tr td:first-child {
                    border: none;
                }

            .invoice footer {
                width: 100%;
                text-align: center;
                color: #777;
                border-top: 1px solid #aaa;
                padding: 8px 0;
            }

        @media print {
            .invoice {
                font-size: 11px !important;
                overflow: hidden !important;
            }

                .invoice footer {
                    position: absolute;
                    bottom: 10px;
                    page-break-after: always;
                }

                .invoice > div:last-child {
                    page-break-before: always;
                }
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpContent" runat="server">
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close " style="margin: unset;" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Order Details</h4>
                </div>
                <div class="modal-body">
                    <div>

                        <div class="form-group">
                            <label for="itmName">Item Name</label>
                            <select class="form-control" id="itmName">
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="unitName">Unit Name</label>
                            <select class="form-control" id="unitName">
                            </select>
                        </div>
                        <div class="form-group ohHide">
                            <label for="itmUnitPrice">Item Unit Price</label>
                            <input type="text" class="form-control" id="itmUnitPrice" placeholder="Item Price">
                        </div>
                        <div class="form-group">
                            <label for="itmQty">Item Quantity</label>
                            <input type="text" class="form-control" id="itmQty" placeholder="Item Quantity">
                        </div>
                        <div class="form-group">
                            <label for="itmDiscount">Item Discount %</label>
                            <input type="text" class="form-control" id="itmDiscount" placeholder="Item Discount">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="btnAddOrder" class="btn btn-success">Add</button>
                </div>
            </div>

        </div>
    </div>
    <div id="invoice">


        <div class="invoice overflow-auto">
            <div style="min-width: 600px">
                <header>
                    <div class="toolbar hidden-print noPrint">
                        <div class="text-right">
                            <button id="printInvoice" class="btn btn-info" style="display: -webkit-inline-box; float: right;"><i class="fa fa-print"></i>Print</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <a target="_blank" href="https://www.compumedical-egypt.com/index.html">
                                <img src="https://www.compumedical-egypt.com/App_Themes/FrontTheme_en-US/images/logo.png" data-holder-rendered="true" />
                            </a>
                        </div>
                    </div>
                </header>
                <main>
                    <div class="row contacts">
                        <div class="col invoice-to">
                            <div class="text-gray-light">INVOICE FROM:</div>
                            <div class="form-group col-md-6 noMargin">
                                <select class="form-control" id="ddlStoreName">
                                </select>
                            </div>
                        </div>
                        <div class="col invoice-details">
                            <h1 class="invoice-id">INVOICE <span class="invoiceNum">anyNum</span></h1>
                            <div class="date">
                                Date of Invoice:
                                <asp:Literal Text="01/10/2018" ID="lblDateNow" runat="server" />
                            </div>
                        </div>
                    </div>

                    <div class="toolbar hidden-print noPrint">
                        <div class="text-right">
                            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Add Order</button>
                        </div>
                        <hr>
                    </div>
                    <table border="0" cellspacing="0" cellpadding="0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th class="text-center">ITEM NAME</th>
                                <th class="text-center">UNIT</th>
                                <th class="text-center">UNIT PRICE</th>
                                <th class="text-center">QTY</th>
                                <th class="text-center">TOTAL</th>
                                <th class="text-center">DISCOUNT</th>
                                <th class="text-center">NET</th>
                            </tr>
                        </thead>
                        <tbody class="myInvoiceBody">
                            <tr>
                                <td class="no">01</td>
                                <td class="text-left">
                                    <h3>Milk
                                    </h3>
                                </td>
                                <td class="unit">Kilo</td>
                                <td class="qty">$<span>12.50</span></td>
                                <td class="unit">2</td>
                                <td class="qty">$<span>25.00</span></td>
                                <td class="unit"><span>20</span>%</td>
                                <td class="total">$<span>20.00</span></td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="5"></td>
                                <td colspan="2">SUBTOTAL</td>
                                <td>$<span class="Suby"></span></td>
                            </tr>
                            <tr>
                                <td colspan="5"></td>
                                <td colspan="2">TAX 25%</td>
                                <td>$<span class="taxy"></span></td>
                            </tr>
                            <tr>
                                <td colspan="5"></td>
                                <td colspan="2">GRAND TOTAL</td>
                                <td>$<span class="grandTotaly"></span></td>
                            </tr>
                        </tfoot>
                    </table>
                    <div class="thanks">Thank you!</div>
                    <div class="notices">
                        <div>NOTICE:</div>
                        <div class="notice">A finance charge of 1.5% will be made on unpaid balances after 30 days.</div>
                    </div>
                </main>
                <footer>
                    Invoice was created on a computer and is valid without the signature and seal.
           
                </footer>
            </div>
            <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->
            <div></div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cpScript" runat="server">

    <script>
        //$(".selectpicker").change(function () {
        //    fillTblFields();
        //});
        $('#printInvoice').click(function () {
            Popup($('.invoice')[0].outerHTML);
            function Popup(data) {
                window.print();
                return true;
            }
        });
        $(document).ready(function () {
            fillStoreNames();
            fillItemNames();
            fillUnitNames();
            fillInvoice();
            //getTotal();
        });

        //initial filling for ddls 
        function fillStoreNames() {
            $("#ddlStoreName").html('');
            $("#ddlStoreName").append($('<option>').attr("value", 0).html('Select Store'));
            $.ajax({
                url: 'Services.asmx/selectGen',
                method: 'post',
                data: { tblNameGen: 'stores' },
                dataType: 'xml',
                success: function (data) {
                    var jqueryXML = $(data);
                    var KeyD = jqueryXML.find("Table").length;

                    for (var i = 0; i < parseInt(KeyD) ; i++) {
                        var rowData = jqueryXML.find("Table").eq(i).find("name").text();
                        var rowVal = jqueryXML.find("Table").eq(i).find("ID").text();

                        $("#ddlStoreName").append($('<option>').attr("value", rowVal).html(rowData));
                    }
                    //$("#itmName").html($("#ddlStoreName").html());
                },
                error: function (err) {
                    alert("error here");

                    //alert("error in retrieving Entity! Sorry");
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('failed in retrieving data, please try again');
                }
            });
            return false;
        }

        function fillItemNames() {
            $("#itmName").html('');
            $("#itmName").append($('<option>').attr("value", 0).html('Select Item'));
            $.ajax({
                url: 'Services.asmx/selectGen',
                method: 'post',
                data: { tblNameGen: 'item' },
                dataType: 'xml',
                success: function (data) {
                    var jqueryXML = $(data);
                    var KeyD = jqueryXML.find("Table").length;

                    for (var i = 0; i < parseInt(KeyD) ; i++) {
                        var rowData = jqueryXML.find("Table").eq(i).find("name").text();
                        var rowVal = jqueryXML.find("Table").eq(i).find("ID").text();

                        $("#itmName").append($('<option>').attr("value", rowVal).html(rowData));
                    }
                    //$("#itmName").html($("#ddlStoreName").html());
                },
                error: function (err) {
                    alert("error here");

                    //alert("error in retrieving Entity! Sorry");
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('failed in retrieving data, please try again');
                }
            });
            return false;
        }

        function fillUnitNames() {
            $("#unitName").html('');
            $("#unitName").append($('<option>').attr("value", 0).html('Select Unit'));
            $.ajax({
                url: 'Services.asmx/selectGen',
                method: 'post',
                data: { tblNameGen: 'units' },
                dataType: 'xml',
                success: function (data) {
                    var jqueryXML = $(data);
                    var KeyD = jqueryXML.find("Table").length;

                    for (var i = 0; i < parseInt(KeyD) ; i++) {
                        var rowData = jqueryXML.find("Table").eq(i).find("name").text();
                        var rowVal = jqueryXML.find("Table").eq(i).find("ID").text();

                        $("#unitName").append($('<option>').attr("value", rowVal).html(rowData));
                    }
                    //$("#itmName").html($("#ddlStoreName").html());
                },
                error: function (err) {
                    alert("error here");

                    //alert("error in retrieving Entity! Sorry");
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('failed in retrieving data, please try again');
                }
            });
            return false;
        }
        function getTotalPerOrder(unitPrice, qty) {
            var total = parseFloat(unitPrice) * parseFloat(qty);
            return total;
        }
        function getNetPerOrder(totalP, discount) {
            var nety = parseFloat(totalP) - (parseFloat(totalP) * parseFloat(discount) / 100);
            return nety;
        }
        // delete column
        function deleteAttr(tableName, AttrName) {

            $.ajax({
                url: 'Service.asmx/deleteCol',
                method: 'POST',
                contentType: 'application/json;charset-utf-8',
                data: JSON.stringify({ tblName: tableName, colName: AttrName }),
                dataType: 'json',
                success: function (data) {
                    //alert(data.d); 
                    var msg = data.d;
                    if (msg.indexOf("Sorry") >= 0) {
                        $('.myAlert.failed').css('display', 'block');
                        $('#failMsg').html('failed to delete this column from ' + tableName + ' table, this column is already in relation as refernce column');
                    }
                    else {
                        $('.myAlert.success').css('display', 'block');
                        $('#succMsg').html(AttrName + 'successfully deleted from ' + tableName + ' table');

                        $.ajax({
                            url: 'Service.asmx/deleteTblHelper',
                            method: 'POST',
                            contentType: 'application/json;charset-utf-8',
                            data: JSON.stringify({ tblNam: tableName, colNam: AttrName }),
                            dataType: 'json',
                            success: function (data) {
                                $('.myAlert.success').css('display', 'block');
                                $('#succMsg').html(' successfully delete ' + AttrName + ' Info from tblHelper');
                            },
                            error: function (err) {
                                //alert(err.responseText);
                                $('.myAlert.failed').css('display', 'block');
                                $('#failMsg').html('failed to delete ' + AttrName + ' Info from tblHelper table, please try again');
                            }
                        });

                    }

                },
                error: function (err) {
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('failed to delete this column from ' + tableName + ' table, please try again');
                }
            });

            //fillTblFields();
            //return false;
        }
        $("#btnAddOrder").click(function () {
            addOrder();
        });
        function addOrderToInvoice(orderID) {
            var storeID = $("#ddlStoreName").val();
            var d = new Date();
            var strDate =  d.getDate() + "-" + (d.getMonth() + 1) + "-" +d.getFullYear() ;
            var txtys = " N'anyNum'," + orderID + " , " + storeID + " , " + strDate;
            $.ajax({
                url: 'Services.asmx/inserGen',
                method: 'POST',
                contentType: 'application/json;charset-utf-8',
                data: JSON.stringify({ tblName: "Invoice", cols: " invoiceNum, Order_ID,storeID,orderDate ", txt: txtys }),
                dataType: 'json',
                success: function (data) {
                    //alert('done');
                    //closeModal();

                },
                error: function (err) {
                    alert("error here");

                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('failed to delete this column from ' + tableName + ' table, please try again');
                }
            });
        }
        // add order
        function addOrder() {
            //var d = new Date();
            //var strDate =  d.getDate() + "/" + (d.getMonth() + 1) + "/" +d.getFullYear() ;
            var itemNameMod = $("#itmName").val();
            var itemUnitMod = $("#unitName").val();
            //var itemPriceMod = $("#itmUnitPrice").val();
            var itemQtyMod = $("#itmQty").val();
            var itemDiscountMod = $("#itmDiscount").val();
            //var totalPrice = (itemQtyMod * itemPriceMod);
            //var totalPriceNet = (totalPrice) - ((itemDiscountMod / 100) * (totalPrice));
            var texty = itemNameMod + ", " + itemUnitMod + ", " + itemQtyMod + ", " + itemDiscountMod;
            var colsy = ' itemID, unitID,  Qty,  discount';

            //alert(strDate);

            $.ajax({
                url: 'Services.asmx/inserGen',
                method: 'POST',
                contentType: 'application/json;charset-utf-8',
                data: JSON.stringify({ tblName: "orders", cols: colsy, txt: texty }),
                dataType: 'json',
                success: function (data) {
                    //alert(data.d);
                    var orderID = data.d;
                    addOrderToInvoice(orderID);
                    fillInvoice();
                    closeModal();

                },
                error: function (err) {
                    alert("error here");

                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('failed to delete this column from ' + tableName + ' table, please try again');
                }
            });

            //return false;
        }
        function getTotal() {
            var total = 0;
            for (var i = 0; i < $(".total").length; i++) {
                total += parseFloat($(".total").eq(i).find("span").text());
            }
            var taxes = total * 0.25;
            var grandy = total + taxes;
            $(".Suby").text(total);
            $(".taxy").text(taxes);
            $(".grandTotaly").text(grandy.toFixed(2));
        }
        function fillInvoice() {
            $.ajax({
                url: 'Services.asmx/selectGen',
                method: 'post',
                data: { tblNameGen: 'ViewInvoice' },
                dataType: 'xml',
                success: function (data) {
                    var jqueryXML = $(data);
                    var KeyD = jqueryXML.find("Table").length;
                    $(".myInvoiceBody").html('');
                    var orderRows = "";
                    for (var i = 0; i < parseInt(KeyD) ; i++) {
                        var MainInvID = jqueryXML.find("Table").eq(i).find("MainInvID").text();
                        var OrderID = jqueryXML.find("Table").eq(i).find("Order_ID").text();
                        var ItemName = jqueryXML.find("Table").eq(i).find("ItemName").text();
                        var unitName = jqueryXML.find("Table").eq(i).find("unitName").text();
                        var unitPrice = jqueryXML.find("Table").eq(i).find("unitPrice").text();
                        var Qty = jqueryXML.find("Table").eq(i).find("Qty").text();
                        var discount = jqueryXML.find("Table").eq(i).find("discount").text();
                        var totalPerOrder = getTotalPerOrder(unitPrice, Qty);
                        var netPerOrder = getNetPerOrder(totalPerOrder, discount);
                        orderRows += '<tr> <td class="no">' + OrderID + '</td> <td class="text-left"> <h3>' + ItemName + '</h3> </td> <td class="unit">' + unitName + '</td> <td class="qty">$<span>' + unitPrice + '</span></td> <td class="unit">' + Qty + '</td> <td class="qty">$<span>' + totalPerOrder + '</span></td> <td class="unit"><span>' + discount + '</span>%</td> <td class="total">$<span>' + netPerOrder + '</span></td> </tr>';
                    }
                    $(".myInvoiceBody").append(orderRows);
                    //$("#itmName").html($("#ddlStoreName").html());
                    getTotal();
                },
                error: function (err) {
                    alert("error here");

                    //alert("error in retrieving Entity! Sorry");
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('failed in retrieving data, please try again');
                }
            });
        }

        function closeModal() {
            $("#myModal").removeClass("show");
            $("#myModal").css("display","none");
            $(".modal-backdrop").removeClass("show");

        }

    </script>

</asp:Content>
