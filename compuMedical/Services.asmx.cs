﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.ComponentModel;
using System.Data;
using System.Web.Script.Services;
using System.Text.RegularExpressions;
using System.Text;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Data.SqlClient;

namespace compuMedical
{
    /// <summary>
    /// Summary description for Services
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Services : System.Web.Services.WebService
    {
        CodeClass codeClass = new CodeClass();

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public DataSet selectGen(string tblNameGen)
        {
            try
            {
                DataSet ds = codeClass.SQLREAD(" select * from " + tblNameGen);
                return ds;

            }
            catch (Exception ex)
            {
                DataSet emptyDS = new DataSet();
                return emptyDS;
            }

        }

        [WebMethod]
        public void deleteGen(string tblName, int idDel)
        {
            try
            {
                if (tblName == "relations")
                {
                    DataSet ds = codeClass.SQLREAD("select * from relations where ID = " + idDel);
                    string tblFrom = ds.Tables[0].Rows[0]["fromTbl"].ToString();
                    string colFrom = ds.Tables[0].Rows[0]["fromCol"].ToString();
                    codeClass.SQLINSERT("Delete from " + tblName + " Where ID = " + idDel);

                    string str = "update tblHelper set FieldType = N'text',  refQuery = NULL where tblName = N'" + tblFrom + "' and colName = N'" + colFrom + "'";
                    codeClass.SQLINSERT(str);
                }
                else
                {
                    if (tblName == "AccessLevel")
                    {
                        codeClass.SQLINSERT("Delete from Permissions Where AccessID = " + idDel);
                    }

                    codeClass.SQLINSERT("Delete from " + tblName + " Where ID = " + idDel);
                }

            }
            catch (Exception ex)
            {

            }
        }

        [WebMethod]
        public int inserGen(string tblName, string cols, string txt)
        {
            try
            {
                int id = codeClass.SQLGetLastID("INSERT INTO " + tblName + " (" + cols + ") VALUES (" + txt + ") SELECT SCOPE_IDENTITY() AS [SCOPE_IDENTITY]");

                return id;
            }
            catch (Exception ex)
            {
                return -1;
            }

        }

        [WebMethod]
        public void updateGen(string tblName, int idUp, string txt)
        {
            try
            {
                //email = '"+Email+"',fName = '"+Fname+ "',lName = '"+Lname+"',address = '"+Address+ "',city = '"+City+ "',about = '"+About+"'
                string queryUp = "Update " + tblName + " SET " + txt + " Where ID = " + idUp;
                codeClass.SQLINSERT(queryUp);
            }
            catch (Exception ex)
            {

            }
        }
        [WebMethod]
        public DataSet selectQuery(string myQuery)
        {
            try
            {
                DataSet ds = codeClass.SQLREAD(myQuery);
                return ds;

            }
            catch (Exception ex)
            {
                DataSet emptyDS = new DataSet();
                return emptyDS;
            }
        }
    }
}
